-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 13, 2022 at 07:14 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shoesstoredatabase`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `Username` varchar(100) NOT NULL,
  `Password` varchar(200) DEFAULT NULL,
  `FullName` varchar(100) DEFAULT NULL,
  `Role` int(5) DEFAULT NULL,
  `Status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`Username`, `Password`, `FullName`, `Role`, `Status`) VALUES
('nghia', '202cb962ac59075b964b07152d234b70', 'nghĩa', 1, 1),
('tannguyenvnkg', '728dd987a042012d97327acf9072ab6f', 'Nguyễn Phước Tấn', 1, 1),
('tannguyenvnkg1', '728dd987a042012d97327acf9072ab6f', 'tấn', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `CustomerID` int(5) NOT NULL,
  `CustomerName` varchar(100) DEFAULT NULL,
  `Username` varchar(100) DEFAULT NULL,
  `Password` varchar(100) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Address` varchar(100) DEFAULT NULL,
  `PhoneNumber` varchar(15) DEFAULT NULL,
  `Birthday` date DEFAULT NULL,
  `ResetCode` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`CustomerID`, `CustomerName`, `Username`, `Password`, `Email`, `Address`, `PhoneNumber`, `Birthday`, `ResetCode`) VALUES
(14, 'Tấn Nguyễn', 'tan2', '728dd987a042012d97327acf9072ab6f', 'tannguyenvnkg2@gmail.com', 'Chung cư Khang Gia 2', '0398160184', '2000-10-25', NULL),
(16, 'Nguyễn Phước Tấn', 'tan', '728dd987a042012d97327acf9072ab6f', 'tannguyenvnkg@gmail.com', 'Quang Trung, Gò Vấp', '0378657588', '2000-11-20', NULL),
(17, 'Nghĩa', '24hghiengame@gmail.com', 'd9b1d7db4cd6e70935368a1efb10e377', '24hghiengame@gmail.com', 'Đồng', '090822222', '2000-01-21', 20307);

-- --------------------------------------------------------

--
-- Table structure for table `orderdetail`
--

CREATE TABLE `orderdetail` (
  `OrderID` int(5) NOT NULL,
  `ProductID` int(5) NOT NULL,
  `SizeID` int(5) NOT NULL,
  `Amount` int(5) DEFAULT NULL,
  `Price` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orderdetail`
--

INSERT INTO `orderdetail` (`OrderID`, `ProductID`, `SizeID`, `Amount`, `Price`) VALUES
(1, 7, 1, 1, 1200000),
(1, 7, 2, 3, 1200000),
(2, 7, 1, 1, 1200000),
(2, 7, 2, 3, 1200000),
(3, 7, 1, 1, 1200000),
(3, 7, 2, 3, 1200000),
(4, 7, 1, 1, 1200000),
(4, 7, 2, 3, 1200000),
(5, 7, 1, 1, 1200000),
(5, 7, 2, 3, 1200000),
(6, 7, 1, 1, 1200000),
(6, 7, 2, 3, 1200000),
(7, 7, 1, 1, 1200000),
(7, 7, 2, 3, 1200000),
(8, 7, 1, 1, 1200000),
(8, 7, 2, 3, 1200000),
(9, 7, 1, 4, 1200000),
(10, 7, 1, 1, 1200000),
(11, 7, 1, 4, 1200000),
(11, 7, 2, 5, 1200000),
(12, 7, 1, 1, 490000),
(12, 7, 4, 1, 490000),
(12, 8, 2, 4, 3200000),
(12, 8, 4, 3, 3200000),
(12, 12, 1, 2, 1390000),
(13, 7, 8, 3, 490000),
(13, 9, 2, 4, 320000),
(13, 12, 1, 1, 1390000),
(13, 13, 6, 2, 1190000);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `ProductID` int(5) NOT NULL,
  `ProductName` varchar(100) DEFAULT NULL,
  `Price` float DEFAULT NULL,
  `Image` varchar(300) DEFAULT NULL,
  `Update_at` datetime DEFAULT NULL,
  `ProviderID` int(5) DEFAULT NULL,
  `TypeID` int(5) DEFAULT NULL,
  `Status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`ProductID`, `ProductName`, `Price`, `Image`, `Update_at`, `ProviderID`, `TypeID`, `Status`) VALUES
(7, 'Vans Old Skool', 490000, 'http://localhost:8000/images/product_image/625693574d173YEEZY-BOOS-350-V2.jpg', '2022-04-13 13:04:04', 6, 1, 1),
(8, 'Converse Chuck', 3200000, 'http://localhost:8000/images/product_image/625692f474a5fSK8-MID-vang.jpg', '2022-03-10 07:30:03', 8, 6, 1),
(9, 'QLD SKOOL ( tím )', 320000, 'http://localhost:8000/images/product_image/62550c7753fcbOLD-SKOOL-tim.png', '2022-04-12 05:21:04', 8, 6, 1),
(10, 'Nghia', 111111, 'http://localhost:8000/images/product_image/625694abba47bULTRABOOST-22-HEAT-trang.jpg', '2022-04-13 09:15:04', 3, 1, 0),
(11, 'ULTRABOOST 22 HEAT ( trắng )', 1200000, 'http://localhost:8000/images/product_image/6256cb071173cULTRABOOST-22-HEAT-trang.jpg', '2022-04-13 13:07:04', 3, 7, 1),
(12, 'SK8 HI-Pig Shede ( cổ cao )', 1390000, 'http://localhost:8000/images/product_image/6256e31d0bae8SK8-HI-Pig-Shede(co cao).jpg', '2022-04-13 14:50:04', 6, 6, 1),
(13, 'SK8-HI ( cổ cao )', 1190000, 'http://localhost:8000/images/product_image/6256f23a007f9SK8-HI(co cao).jpg', '2022-04-13 15:54:04', 6, 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `productdetail`
--

CREATE TABLE `productdetail` (
  `ProductID` int(5) NOT NULL,
  `SizeID` int(5) NOT NULL,
  `QuantityOfStock` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `productdetail`
--

INSERT INTO `productdetail` (`ProductID`, `SizeID`, `QuantityOfStock`) VALUES
(7, 1, 499),
(7, 2, 500),
(7, 3, 0),
(7, 4, 499),
(7, 5, 0),
(7, 6, 600),
(7, 7, 0),
(7, 8, 97),
(8, 1, 0),
(8, 2, 141),
(8, 3, 0),
(8, 4, 253),
(8, 5, 0),
(8, 6, 0),
(8, 7, 0),
(8, 8, 0),
(9, 1, 0),
(9, 2, 311),
(9, 3, 0),
(9, 4, 0),
(9, 5, 411),
(9, 6, 0),
(9, 7, 0),
(9, 8, 0),
(9, 9, 0),
(10, 1, 0),
(10, 2, 0),
(10, 3, 0),
(10, 4, 0),
(10, 5, 0),
(10, 6, 0),
(10, 7, 0),
(10, 8, 0),
(10, 9, 0),
(11, 1, 0),
(11, 2, 0),
(11, 3, 0),
(11, 4, 0),
(11, 5, 0),
(11, 6, 0),
(11, 7, 0),
(11, 8, 0),
(11, 9, 0),
(11, 10, 0),
(11, 11, 0),
(11, 12, 0),
(11, 13, 0),
(11, 14, 0),
(11, 15, 0),
(12, 1, 231),
(12, 2, 0),
(12, 3, 0),
(12, 4, 111),
(12, 5, 0),
(12, 6, 444),
(12, 7, 0),
(12, 8, 0),
(12, 9, 0),
(12, 10, 0),
(12, 11, 0),
(12, 12, 0),
(12, 13, 0),
(12, 14, 0),
(12, 15, 0),
(13, 1, 369),
(13, 2, 0),
(13, 3, 428),
(13, 4, 0),
(13, 5, 213),
(13, 6, 610),
(13, 7, 0),
(13, 8, 451),
(13, 9, 0),
(13, 10, 0),
(13, 11, 0),
(13, 12, 0),
(13, 13, 0),
(13, 14, 0),
(13, 15, 0);

-- --------------------------------------------------------

--
-- Table structure for table `productsize`
--

CREATE TABLE `productsize` (
  `SizeID` int(5) NOT NULL,
  `SizeNumber` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `productsize`
--

INSERT INTO `productsize` (`SizeID`, `SizeNumber`) VALUES
(1, 36),
(2, 37),
(3, 38),
(4, 39),
(5, 40),
(6, 41),
(7, 42),
(8, 43),
(9, 44),
(10, 45),
(11, 46),
(12, 47),
(13, 48),
(14, 47),
(15, 48);

-- --------------------------------------------------------

--
-- Table structure for table `provider`
--

CREATE TABLE `provider` (
  `ProviderID` int(5) NOT NULL,
  `ProviderName` varchar(100) DEFAULT NULL,
  `Address` varchar(300) DEFAULT NULL,
  `PhoneNumber` varchar(15) DEFAULT NULL,
  `Status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `provider`
--

INSERT INTO `provider` (`ProviderID`, `ProviderName`, `Address`, `PhoneNumber`, `Status`) VALUES
(1, 'Nike', '31 Võ Văn Ngân', '0990098', 1),
(2, 'Bitis', '32 Bà Chiểu', '098044', 1),
(3, 'Adidas', '35 Điện Biên Phủ', '0944235', 1),
(4, 'Puma', '78 Võ Văn Kiệt', '0983474', 1),
(5, 'Ananas', '31 Nguyễn Ái Quốc', '0758416231', 1),
(6, 'Vans', 'Quang Trung, Gò Vấp', '0564318921', 1),
(8, 'Converse', 'Nguyễn Kiệm, Gò Vấp', '09037891753', 1),
(9, 'Ananimus', '47 Phan Văn Trung', '0865333142', 1);

-- --------------------------------------------------------

--
-- Table structure for table `theorder`
--

CREATE TABLE `theorder` (
  `OrderID` int(5) NOT NULL,
  `Paid` tinyint(1) DEFAULT NULL,
  `DeliveryStatus` tinyint(1) DEFAULT NULL,
  `OrderDate` date DEFAULT NULL,
  `DeliveryDate` date DEFAULT NULL,
  `Address` varchar(500) DEFAULT NULL,
  `TotalMoney` float DEFAULT NULL,
  `NameCustomer` varchar(200) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Phone` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `theorder`
--

INSERT INTO `theorder` (`OrderID`, `Paid`, `DeliveryStatus`, `OrderDate`, `DeliveryDate`, `Address`, `TotalMoney`, `NameCustomer`, `Email`, `Phone`) VALUES
(1, 0, 0, '2022-03-28', '2022-04-13', '0393765782', 4800000, '127/83 Lý Tự Trọng, Gò Vấp', 'Nguyễn Phước Tấn', 'tannguyenvnkg@gmail.com'),
(2, 0, NULL, '2022-03-28', NULL, '0393765782', 4800000, '127/83 Lý Tự Trọng, Gò Vấp', NULL, 'tannguyenvnkg@gmail.com'),
(3, 0, NULL, '2022-03-28', NULL, '0393765782', 4800000, '127/83 Lý Tự Trọng, Gò Vấp', NULL, 'tannguyenvnkg@gmail.com'),
(4, 0, NULL, '2022-03-28', NULL, '0393765782', 4800000, '127/83 Lý Tự Trọng, Gò Vấp', NULL, 'tannguyenvnkg@gmail.com'),
(5, 0, NULL, '2022-03-28', NULL, '0393765782', 4800000, '127/83 Lý Tự Trọng, Gò Vấp', NULL, 'tannguyenvnkg@gmail.com'),
(6, 0, NULL, '2022-03-28', NULL, '0393765782', 4800000, '127/83 Lý Tự Trọng, Gò Vấp', NULL, 'tannguyenvnkg@gmail.com'),
(7, 0, NULL, '2022-03-28', NULL, '0393765782', 4800000, '127/83 Lý Tự Trọng, Gò Vấp', 'a', 'tannguyenvnkg@gmail.com'),
(8, 1, 1, '2022-03-28', '2022-04-13', '127/83 Lý Tự Trọng, Gò Vấp', 4800000, 'a', 'tannguyenvnkg@gmail.com', '0393765782'),
(9, 1, 1, '2022-04-12', '2022-04-13', '123', 4800000, 'Nguyễn Trọng Nghĩa', '24hghiengame@gmail.com', '123456789'),
(10, 1, 1, '2022-04-12', '2022-04-13', 'Đồng', 1200000, 'Nghĩa', '24hghiengame@gmail.com', '090822222'),
(11, 0, 0, '2022-04-12', '2022-04-13', 'Đồng', 10800000, 'Nghĩa', '24hghiengame@gmail.com', '090822222'),
(12, 1, 1, '2022-04-13', '2022-04-13', 'Đồng', 26160000, 'Nghĩa', '24hghiengame@gmail.com', '090822222'),
(13, 0, 0, '2022-04-13', '2022-04-13', '123 Phan Văn Trị', 6520000, 'Mina', 'ngtrongnghia2201@gmail.com', '0931234123');

-- --------------------------------------------------------

--
-- Table structure for table `typeofshoes`
--

CREATE TABLE `typeofshoes` (
  `TypeID` int(5) NOT NULL,
  `NameOfType` varchar(100) DEFAULT NULL,
  `Status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `typeofshoes`
--

INSERT INTO `typeofshoes` (`TypeID`, `NameOfType`, `Status`) VALUES
(1, 'Nam', 1),
(2, 'Nữ', 1),
(3, 'Thể thao', 1),
(4, 'Sandal', 1),
(6, 'Giày Cổ Cao', 1),
(7, 'Giày Cổ Thấp', 1),
(8, 'Giày Cao Gót', 1),
(9, 'Dép', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`Username`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`CustomerID`);

--
-- Indexes for table `orderdetail`
--
ALTER TABLE `orderdetail`
  ADD PRIMARY KEY (`OrderID`,`ProductID`,`SizeID`),
  ADD KEY `ProductID` (`ProductID`),
  ADD KEY `SizeID` (`SizeID`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`ProductID`),
  ADD KEY `ProviderID` (`ProviderID`),
  ADD KEY `TypeID` (`TypeID`);

--
-- Indexes for table `productdetail`
--
ALTER TABLE `productdetail`
  ADD PRIMARY KEY (`ProductID`,`SizeID`),
  ADD KEY `SizeID` (`SizeID`);

--
-- Indexes for table `productsize`
--
ALTER TABLE `productsize`
  ADD PRIMARY KEY (`SizeID`);

--
-- Indexes for table `provider`
--
ALTER TABLE `provider`
  ADD PRIMARY KEY (`ProviderID`);

--
-- Indexes for table `theorder`
--
ALTER TABLE `theorder`
  ADD PRIMARY KEY (`OrderID`);

--
-- Indexes for table `typeofshoes`
--
ALTER TABLE `typeofshoes`
  ADD PRIMARY KEY (`TypeID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `CustomerID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `ProductID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `productsize`
--
ALTER TABLE `productsize`
  MODIFY `SizeID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `provider`
--
ALTER TABLE `provider`
  MODIFY `ProviderID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `theorder`
--
ALTER TABLE `theorder`
  MODIFY `OrderID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `typeofshoes`
--
ALTER TABLE `typeofshoes`
  MODIFY `TypeID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orderdetail`
--
ALTER TABLE `orderdetail`
  ADD CONSTRAINT `orderdetail_ibfk_1` FOREIGN KEY (`OrderID`) REFERENCES `theorder` (`OrderID`),
  ADD CONSTRAINT `orderdetail_ibfk_2` FOREIGN KEY (`ProductID`) REFERENCES `product` (`ProductID`),
  ADD CONSTRAINT `orderdetail_ibfk_3` FOREIGN KEY (`SizeID`) REFERENCES `productsize` (`SizeID`);

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`ProviderID`) REFERENCES `provider` (`ProviderID`),
  ADD CONSTRAINT `product_ibfk_2` FOREIGN KEY (`TypeID`) REFERENCES `typeofshoes` (`TypeID`);

--
-- Constraints for table `productdetail`
--
ALTER TABLE `productdetail`
  ADD CONSTRAINT `productdetail_ibfk_1` FOREIGN KEY (`ProductID`) REFERENCES `product` (`ProductID`),
  ADD CONSTRAINT `productdetail_ibfk_2` FOREIGN KEY (`SizeID`) REFERENCES `productsize` (`SizeID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
