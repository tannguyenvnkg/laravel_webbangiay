<?php

namespace App\Models;

use App\Models\Admin as ModelsAdmin;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Admin extends Model
{
    use HasFactory;
    protected $table = 'admin';
    private $username;
    private $password;
    private $fullName;
    private $role;
    private $status;

    /*
    ------------------------------------------------------------------------
    GET SET CONSTRUCT
    ------------------------------------------------------------------------
    */
    public static function emptyConstruct(){return new Admin('','','','','','','');}
    public function __construct($username,$password,$fullName,$role,$status)
    {
        $this->username = $username;
        $this->password = $password;
        $this->fullName = $fullName;
        $this->role = $role;
        $this->status = $status;
    }

    public function getUsername(){
		return $this->username;
	}

	public function setUsername($username){
		$this->username = $username;
	}

	public function getPassword(){
		return $this->password;
	}

	public function setPassword($password){
		$this->password = $password;
	}

	public function getFullName(){
		return $this->fullName;
	}

	public function setFullName($fullName){
		$this->fullName = $fullName;
	}

	public function getRole(){
		return $this->role;
	}

	public function setRole($role){
		$this->role = $role;
	}

	public function getStatus(){
		return $this->status;
	}

	public function setStatus($status){
		$this->status = $status;
	}
    /*
    ------------------------------------------------------------------------
    ------------------------------------------------------------------------
    ------------------------------------------------------------------------
    */

    public function login(){
        $data = DB::table($this->table)
        ->where('Username', $this->getUsername())
        ->where('Password', $this->getPassword())
        ->first();
        return $data;
    }

    public function addnewadmin(){
        if(($checkAccount = $this->checkAccount())){ // if username is already registered
            return $checkAccount;
        }
        $data = DB::table($this->table)
        ->insert([
            'Username' => $this->getUsername(),
            'Password' => $this->getPassword(),
            'FullName' => $this->getFullName(),
            'Role' => $this->getRole(),
            'Status' => $this->getStatus(),
        ]);
        if($data)
            return array(
                'error' => false,
                'message' => 'thêm tài khoản thành công'
            );
        else 
            return array(
                'error' => true,
                'message' => 'thêm tài khoản thất bại'
            );

        return $data;
    }

    private function checkAccount(){ // check if username is allready exits
        $data = DB::table($this->table)
        ->where('Username', $this->getUsername())
        ->first();
        if($data !==null){ // exist username
            return array(
                'error' => true,
                'message' => 'thêm tài khoản thất bại, tài khoản đã được đăng ký'
            );
        }
    }

    private function findAdmin($username){
        $data = DB::table($this->table)
        ->where('Username', $username)
        ->first();
        return $data;
    }

    public function deactivateAdmin($loggedAdminUsername,$deactivatedAdminUsername)
    {
        $loggedAdmin = $this->findAdmin($loggedAdminUsername);
        $deactivatedAdmin = $this->findAdmin($deactivatedAdminUsername);
        if($loggedAdmin===null || $deactivatedAdmin===null){
            return array(
                'error' => true,
                'message' => 'tài khoản không tồn tại'
            );
        }else{
            if($loggedAdmin->Role===1 && ($loggedAdmin->Role < $deactivatedAdmin->Role)){
                $data = DB::table($this->table)
                ->where('Username',$deactivatedAdminUsername)
                ->update([ // update
                    'Status' => 0
                ]);

                if($data)
                    return array(
                        'error' => false,
                        'message' => 'khóa tài khoản thành công',
                    );
                else 
                    return array(
                        'error' => true,
                        'message' => 'khóa tài khoản thất bại'
                    );
            }else{
                return array(
                    'error' => true,
                    'message' => 'Bạn không có quyền ngưng hoạt động tài khoản này'
                );
            }
        }
    }
}
