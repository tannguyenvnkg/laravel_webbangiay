<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Category extends Model
{
    use HasFactory;
    protected $table = 'typeofshoes';
    private $categoryName;
    private $status;

    /*
    ------------------------------------------------------------------------
    GET SET CONSTRUCT
    ------------------------------------------------------------------------
    */
    public static function emptyConstruct(){return new Category('',1);}
    public function __construct($categoryName,$status)
    {
        $this->categoryName = $categoryName;
        $this->status = $status;
    }

    public function getCategoryName(){
		return $this->categoryName;
	}

	public function setCategoryName($categoryName){
		$this->categoryName = $categoryName;
	}

	public function getStatus(){
		return $this->status;
	}

	public function setStatus($status){
		$this->status = $status;
	}
    /*  
    ------------------------------------------------------------------------
    ------------------------------------------------------------------------
    ------------------------------------------------------------------------
    */

    public function getAllCategory(){
        $data = DB::table($this->table)->get();
        return $data;
    }

    public function addCategory(){
        $data = DB::table($this->table)->insert([
            'NameOfType' => $this->getCategoryName(),
            'Status' => $this->getStatus(),
        ]);
        if($data)
            return array(
                'error' => false,
                'message' => 'thêm loại giày thành công'
            );
        else 
            return array(
                'error' => true,
                'message' => 'thêm loại giày thất bại'
            );

        return $data;
    }

    public function editCategory($categoryId){
        $category = $this->findCategory($categoryId);
        if($category===null){ // if there is no category found
            return array('error' => true,'message' => 'Không tìm thấy loại giày');
        }else{
            if($category->NameOfType == $this->getCategoryName())
            return array(
                'error' => true,
                'message' => 'cập nhật thất bại, sản phẩm không có thay đổi'
            );

            $data = DB::table($this->table)->where('TypeID',$categoryId)->update([ // update
                'NameOfType' => $this->getCategoryName(),
                'Status' => $this->getStatus(),
            ]);

            if($data)
                return array(
                    'error' => false,
                    'message' => 'cập nhật loại giày thành công'
                );
            else 
                return array(
                    'error' => true,
                    'message' => 'cập nhật loại giày thất bại'
                );
        }
    }

    public function findCategory($categoryId){
        $data = DB::table($this->table)->where('TypeID', $categoryId)->first();
        return $data;
    }
}
