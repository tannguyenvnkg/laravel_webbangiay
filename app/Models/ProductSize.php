<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class ProductSize extends Model
{
    use HasFactory;
    protected $table = 'productsize';
    private $sizeNumber;

    /*
    ------------------------------------------------------------------------
    GET SET CONSTRUCT
    ------------------------------------------------------------------------
    */
    public static function emptyConstruct(){return new ProductSize('');}
    public function __construct($sizeNumber)
    {
        $this->sizeNumber = $sizeNumber;
    }
    public function getSizeNumber(){
		return $this->sizeNumber;
	}

	public function setSizeNumber($sizeNumber){
		$this->sizeNumber = $sizeNumber;
	}
    /*
    ------------------------------------------------------------------------
    ------------------------------------------------------------------------
    ------------------------------------------------------------------------
    */

    public function getAllProductSize(){
        $data = DB::table($this->table)->get();
        return $data;
    }

    public function addProductSize(){
        $data = DB::table($this->table)->insert([
            'SizeNumber' => $this->getSizeNumber(),
        ]);
        if($data)
            return array(
                'error' => false,
                'message' => 'thêm size sản phẩm thành công'
            );
        else 
            return array(
                'error' => true,
                'message' => 'thêm size sản phẩm thất bại'
            );

        return $data;
    }

    public function editProductSize($productSizeId){
        $productSize = $this->findProductSize($productSizeId);
        if($productSize===null){ // if there is no productsize found
            return array('error' => true,'message' => 'Không tìm thấy size giày');
        }else{
            if($productSize->SizeNumber == $this->getSizeNumber())
                return array(
                    'error' => true,
                    'message' => 'cập nhật thất bại, size giày không có thay đổi'
                );

            $data = DB::table($this->table)->where('SizeID',$productSizeId)->update([ // update
                'SizeNumber' => $this->getSizeNumber(),
            ]);

            if($data)
                return array(
                    'error' => false,
                    'message' => 'cập nhật size giày thành công'
                );
            else 
                return array(
                    'error' => true,
                    'message' => 'cập nhật size giày thất bại'
                );
        }
    }

    public function findProductSize($productSizeId){
        $data = DB::table($this->table)->where('SizeID', $productSizeId)->first();
        return $data;
    }
}
