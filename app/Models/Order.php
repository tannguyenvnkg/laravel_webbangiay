<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Order extends Model
{
    use HasFactory;
    protected $table = 'theorder';
    protected $customerTable = 'customer';
    private $paid;
    private $deliveryStatus;
    private $orderDate;
    private $deliveryDate;
    private $totalMoney;
    private $nameCustomer;
    private $email;
    private $phone;
    private $address;
    
    /*
    ------------------------------------------------------------------------
    GET SET CONSTRUCT
    ------------------------------------------------------------------------
    */
    public static function emptyConstruct(){return new Order(null,null,null,null,0,null,null,null,null);}
    public function __construct($paid,$deliveryStatus,$orderDate,$deliveryDate,$totalMoney,$nameCustomer,$email,$phone,$address)
    {
        $this->paid = $paid;
        $this->deliveryStatus = $deliveryStatus;
        $this->orderDate = $orderDate;
        $this->deliveryDate = $deliveryDate;
        $this->nameCustomer = $nameCustomer;
        $this->email = $email;
        $this->phone = $phone;
        $this->address = $address;
        $this->totalMoney = $totalMoney;
    }
    public function getPaid(){
		return $this->paid;
	}

	public function setPaid($paid){
		$this->paid = $paid;
	}

	public function getDeliveryStatus(){
		return $this->deliveryStatus;
	}

	public function setDeliveryStatus($deliveryStatus){
		$this->deliveryStatus = $deliveryStatus;
	}

	public function getOrderDate(){
		return $this->orderDate;
	}

	public function setOrderDate($orderDate){
		$this->orderDate = $orderDate;
	}

	public function getDeliveryDate(){
		return $this->deliveryDate;
	}

	public function setDeliveryDate($deliveryDate){
		$this->deliveryDate = $deliveryDate;
	}

    public function getNameCustomer(){
		return $this->nameCustomer;
	}

	public function setNameCustomer($nameCustomer){
		$this->nameCustomer = $nameCustomer;
	}

	public function getEmail(){
		return $this->email;
	}

	public function setEmail($email){
		$this->email = $email;
	}

	public function getPhone(){
		return $this->phone;
	}

	public function setPhone($phone){
		$this->phone = $phone;
	}

	public function getAddress(){
		return $this->address;
	}

	public function setAddress($address){
		$this->address = $address;
	}

	public function getTotalMoney(){
		return $this->totalMoney;
	}

	public function setTotalMoney($totalMoney){
		$this->totalMoney = $totalMoney;
	}

	
    /*
    ------------------------------------------------------------------------
    ------------------------------------------------------------------------
    ------------------------------------------------------------------------
    */

    public function addOrderAndGetOrderID(){
        $id = DB::table($this->table)
        ->insertGetId([
            'Paid' => $this->getPaid(),
            'DeliveryStatus' => $this->getDeliveryStatus(),
            'OrderDate' => $this->getOrderDate(),
            'DeliveryDate' => $this->getDeliveryDate(),
            'Address' => $this->getAddress(),
            'TotalMoney' => $this->getTotalMoney(),
            'NameCustomer' => $this->getNameCustomer(),
            'Email' => $this->getEmail(),
            'Phone' => $this->getPhone()
        ]);
        return $id;
    }

    // private function checkAccount(){ // return true if customer's id is allready exits
    //     $data = DB::table($this->customerTable)
    //     ->where('CustomerID', $this->getCustomerID())
    //     ->first();
    //     if($data !==null){ // exist customer's id
    //         return true;
    //     }
    //     return false;
    // }

    public function getListCustomerOrder($customerId)
    {
        $data = DB::table($this->table)
        ->where('CustomerID', $customerId)
        ->get();
        return $data;
    }

    public function findOrder($id)
    {
        $data = DB::table($this->table)
        ->where('OrderID', $id)
        ->first();
        return $data;
    }

    public function deliverySuccess($id){
        if($this->findOrder($id)===null){ // check order's id is existed
            return array(
                'error' => true,
                'message' => 'Không tìm thấy đơn hàng',
            );
        }else{
            $data = DB::table($this->table)
            ->where('OrderID',$id)
            ->update([ // update
                'Paid' => 1,
                'DeliveryStatus' => 1,
                'DeliveryDate' => date('Y-m-d')
            ]);
            if($data)
                return array(
                    'error' => false,
                    'message' => 'giao hàng thành công',
                );
            else 
                return array(
                    'error' => true,
                    'message' => 'giao hàng thất bại'
                );
        }
    }

    public function deliveryCancel($id){
        if($this->findOrder($id)===null){ // check order's id is existed
            return array(
                'error' => true,
                'message' => 'Không tìm thấy đơn hàng',
            );
        }else{
            $data = DB::table($this->table)
            ->where('OrderID',$id)
            ->update([ // update
                'Paid' => 0,
                'DeliveryStatus' => 0,
                'DeliveryDate' => date('Y-m-d')
            ]);
            if($data)
                return array(
                    'error' => false,
                    'message' => 'Hủy đơn hàng thành công',
                );
            else 
                return array(
                    'error' => true,
                    'message' => 'Hủy đơn hàng thất bại'
                );
        }
    }

    public function getOrderSuccessfully()
    {
        $data = DB::table($this->table)
        ->orderByDesc('OrderDate')
        ->where('DeliveryStatus', true)
        ->get();
        if($data)
            return array(
                'error' => false,
                'message' => '',
                'data' => $data
            );
        else 
            return array(
                'error' => true,
                'message' => 'Tìm thất bại'
            );
    }

    public function getOrderCanceled()
    {
        $data = DB::table($this->table)
        ->orderByDesc('OrderDate')
        ->where('DeliveryStatus', false)
        ->get();
        if($data)
            return array(
                'error' => false,
                'message' => '',
                'data' => $data
            );
        else 
            return array(
                'error' => true,
                'message' => 'Tìm thất bại'
            );
    }
    
    public function getOrderdelivering()
    {
        $data = DB::table($this->table)
        ->orderByDesc('OrderDate')
        ->where('DeliveryStatus', null)
        ->get();
        if($data)
            return array(
                'error' => false,
                'message' => '',
                'data' => $data
            );
        else 
            return array(
                'error' => true,
                'message' => 'Tìm thất bại'
            );
    }

    public function findOrderById($orderId)
    {
        $data = DB::table($this->table)
        ->where('OrderID', $orderId)
        ->orWhere('Email', $orderId)
        ->get();
        if($data !== null){
            return array(
                'error' => false,
                'message' => '',
                'data' => $data
            );
        }else{
            return array(
                'error' => true,
                'message' => 'Không tìm thấy đơn hàng'
            );
        }
    }
}
