<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Order;
class OrderDetail extends Model
{
    use HasFactory;
    protected $table = 'orderdetail';
    private $orderID;
    private $productID;
    private $sizeID;
    private $amount;
    private $price;
    
    /*
    ------------------------------------------------------------------------
    GET SET CONSTRUCT
    ------------------------------------------------------------------------
    */
    public static function emptyConstruct(){return new OrderDetail(null,null,null,null,0,null);}
    public function __construct($orderID,$productID,$sizeID,$amount,$price)
    {
        $this->orderID = $orderID;
        $this->productID = $productID;
        $this->sizeID = $sizeID;
        $this->amount = $amount;
        $this->price = $price;
    }

    public function getOrderID(){
		return $this->orderID;
	}

	public function setOrderID($orderID){
		$this->orderID = $orderID;
	}

	public function getProductID(){
		return $this->productID;
	}

	public function setProductID($productID){
		$this->productID = $productID;
	}

	public function getSizeID(){
		return $this->sizeID;
	}

	public function setSizeID($sizeID){
		$this->sizeID = $sizeID;
	}

	public function getAmount(){
		return $this->amount;
	}

	public function setAmount($amount){
		$this->amount = $amount;
	}

	public function getPrice(){
		return $this->price;
	}

	public function setPrice($price){
		$this->price = $price;
	}
    /*
    ------------------------------------------------------------------------
    ------------------------------------------------------------------------
    ------------------------------------------------------------------------
    */

    public function addOrderDetail()
    {
        DB::table($this->table)
        ->insert([
            'OrderID' => $this->getOrderID(),
            'ProductID' => $this->getProductID(),
            'SizeID' => $this->getSizeID(),
            'Amount' => $this->getAmount(),
            'Price' => $this->getPrice(),
        ]);
    }

    public function getCustomerOrderDetail($orderID)
    {
        $data = DB::table($this->table)
        ->join('product',$this->table.'.ProductID', '=', 'product.ProductID')
        ->where('OrderID', $orderID)
        ->get();
        return $data;
    }
    public function getDetailOrder($orderID)
    {
        $order = Order::emptyConstruct();
        $existOrder = $order->findOrder($orderID);
        if($existOrder !== null){
            $data = DB::table($this->table)
            ->join('product',$this->table.'.ProductID', '=', 'product.ProductID')
            ->where('OrderID', $orderID)
            ->get();
            if($data)
                return array(
                    'error' => false,
                    'message' => '',
                    'data' => $data
                );
            else 
                return array(
                    'error' => true,
                    'message' => 'Đơn hàng trống'
                );
        }else{
            return array(
                'error' => true,
                'message' => 'Đơn hàng không tồn tại'
            );
        }
        
    }
}
