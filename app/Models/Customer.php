<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Customer extends Model
{
    use HasFactory;
    protected $table = 'customer';
    private $customerName;
    private $username;
    private $password;
    private $email;
    private $address;
    private $phoneNumber;
    private $birthday;

    /*
    ------------------------------------------------------------------------
    GET SET CONSTRUCT
    ------------------------------------------------------------------------
    */
    public static function emptyConstruct(){return new Customer('','','','','','','');}
    public function __construct($customerName,$username,$password,$email,$address,$phoneNumber,$birthday)
    {
        $this->customerName = $customerName;
        $this->username = $username;
        $this->password = $password;
        $this->email = $email;
        $this->address = $address;
        $this->phoneNumber = $phoneNumber;
        $this->birthday = $birthday;
    }

	public function getCustomerName(){
		return $this->customerName;
	}

	public function setCustomerName($customerName){
		$this->customerName = $customerName;
	}

	public function getUsername(){
		return $this->username;
	}

	public function setUsername($username){
		$this->username = $username;
	}

	public function getPassword(){
		return $this->password;
	}

	public function setPassword($password){
		$this->password = $password;
	}

	public function getEmail(){
		return $this->email;
	}

	public function setEmail($email){
		$this->email = $email;
	}

	public function getAddress(){
		return $this->address;
	}

	public function setAddress($address){
		$this->address = $address;
	}

	public function getPhoneNumber(){
		return $this->phoneNumber;
	}

	public function setPhoneNumber($phoneNumber){
		$this->phoneNumber = $phoneNumber;
	}

	public function getBirthday(){
		return $this->birthday;
	}

	public function setBirthday($birthday){
		$this->birthday = $birthday;
	}
    /*
    ------------------------------------------------------------------------
    ------------------------------------------------------------------------
    ------------------------------------------------------------------------
    */

    public function login(){
        $data = DB::table($this->table)
        ->where(function($query){
            $query->where('Username', $this->getUsername())
                  ->orWhere('Email', $this->getEmail());
        })
        ->where('Password', $this->getPassword())
        ->first();
        return $data;
    }

    public function signup(){
        if(($checkAccount = $this->checkAccount())){ // if username is already registered
            return $checkAccount;
        }
        if(($checkEmailAccount = $this->checkEmailAccount())){ // if email is already registered
            return $checkEmailAccount;
        }
        $data = DB::table($this->table)
        ->insert([
            'CustomerName' => $this->getCustomerName(),
            'Username' => $this->getUsername(),
            'Password' => $this->getPassword(),
            'Email' => $this->getEmail(),
            'Address' => $this->getAddress(),
            'PhoneNumber' => $this->getPhoneNumber(),
            'Birthday' => date('Y-m-d', strtotime($this->getBirthday()))
        ]);
        if($data)
            return array(
                'error' => false,
                'message' => 'đăng ký tài khoản thành công'
            );
        else 
            return array(
                'error' => true,
                'message' => 'đăng ký tài khoản thất bại'
            );

        return $data;
    }

    private function checkAccount(){ // check if username is allready exits
        $data = DB::table($this->table)
        ->where('Username', $this->getUsername())
        ->first();
        if($data !==null){ // exist username
            return array(
                'error' => true,
                'message' => 'đăng ký tài khoản thất bại, tài khoản đã được đăng ký'
            );
        }
    }
    private function checkEmailAccount(){ // check if email is already exits
        $data = DB::table($this->table)
        ->where('Email', $this->getEmail())
        ->first();
        if($data !==null){ // exist username
            return array(
                'error' => true,
                'message' => 'đăng ký tài khoản thất bại, email đã được đăng ký'
            );
        }
    }

    public function editCustomer($customerId){
        $customer = $this->findCustomer($customerId);
        if($customer===null){ // if there is no customer found
            return array('error' => true,'message' => 'Không tìm thấy người dùng');
        }else{

            if($customer->CustomerName == $this->getCustomerName()
            && $customer->Address == $this->getAddress()
            && $customer->PhoneNumber == $this->getPhoneNumber()
            && $customer->Birthday == date('Y-m-d', strtotime($this->getBirthday())))
            return array(
                'error' => true,
                'message' => 'cập nhật thất bại, không có thay đổi'
            );

            $data = DB::table($this->table)
            ->where('CustomerID',$customerId)
            ->update([ // update
                'CustomerName' => $this->getCustomerName(),
                // 'Email' => $this->getEmail(),
                'Address' => $this->getAddress(),
                'PhoneNumber' => $this->getPhoneNumber(),
                'Birthday' => date('Y-m-d', strtotime($this->getBirthday()))
            ]);
            
            if($data)
                return array(
                    'error' => false,
                    'message' => 'cập nhật người dùng thành công',
                    'data' => $this->findCustomer($customerId)
                );
            else 
                return array(
                    'error' => true,
                    'message' => 'cập nhật người dùng thất bại'
                );
        }
    }

    public function findCustomer($customerId){
        $data = DB::table($this->table)->where('CustomerID', $customerId)->first();
        return $data;
    }

    public function changePassword($customerId,$oldPassword){
        $customer = $this->findCustomer($customerId);
        if($customer===null){ // if there is no customer found
            return array('error' => true,'message' => 'Không tìm thấy người dùng');
        }else{
            if($customer->Password==md5($oldPassword)){ 
                if($customer->Password==md5($this->getPassword())){
                    return array(
                        'error' => true,
                        'message' => 'mật khẩu mới phải khác mật khẩu cũ'
                    ); 
                }
                $data = DB::table($this->table)
                ->where('CustomerID',$customerId)
                ->update([ // update
                    'Password' => md5($this->getPassword())
                ]);
                if($data)
                    return array(
                        'error' => false,
                        'message' => 'thay đổi mật khẩu thành công',
                        'data' => $this->findCustomer($customerId)
                    );
                else 
                    return array(
                        'error' => true,
                        'message' => 'thay đổi mật khẩu thất bại'
                    );
            }else{
                return array(
                    'error' => true,
                    'message' => 'mật khẩu không khớp'
                );
            }
        }
    }

    public function addResetCode($email,$OTP)
    {
        $customer = $this->findCustomerByEmail($email);
        if($customer===null){ // if there is no customer found
            return array('error' => true,'message' => 'Không tìm thấy người dùng');
        }else{
            $data = DB::table($this->table)
            ->where('Email',$email)
            ->update([
                'ResetCode' => $OTP
            ]);
            if($data)
                return array('error' => false,'message' => 'Thêm mã OTP thành công');
            else
                return array('error' => true,'message' => 'Thêm mã OTP thất bại');
        }
    }

    public function findCustomerByEmail($email)
    {
        $data = DB::table($this->table)->where('Email', $email)->first();
        return $data;
    }

    public function resetPassword($otp)
    {
        $customer = $this->findCustomerByEmail($this->getEmail());
        if($customer===null){ // if there is no customer found
            return array('error' => true,'message' => 'Không tìm thấy người dùng');
        }else{
            if($customer->ResetCode == $otp){
                $data = DB::table($this->table)
                ->where('Email', $this->getEmail())
                ->update([
                    'Password' => md5($this->getPassword()),
                    'ResetCode' => null
                ]);
                if($data)
                    return array('error' => false,'message' => 'thay đổi mật khẩu thành công');
                else
                    return array('error' => true,'message' => 'thay đổi mật khẩu thất bại');
            }else{
                return array('error' => true,'message' => 'mã otp không chính xác');
            }
        }
    }

    public function checkResetCode($otp)
    {
        $customer = $this->findCustomerByEmail($this->getEmail());
        if($customer===null){ // if there is no customer found
            return array('error' => true,'message' => 'Không tìm thấy người dùng');
        }else{
            if($customer->ResetCode == $otp){
                return array('error' => false,'message' => 'mã otp chính xác');
            }else{
                return array('error' => true,'message' => 'mã otp không chính xác');
            }
        }
    }
}
