<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\ProductSize;

class ProductDetail extends Model
{
    use HasFactory;
    protected $table = 'productdetail';
    private $productID;
    private $sizeID;
    private $quantityOfStock;
    /*
    ------------------------------------------------------------------------
    GET SET CONSTRUCT
    ------------------------------------------------------------------------
    */
    public static function emptyConstruct(){return new ProductDetail('','','');}
    public function __construct($productID,$sizeID,$quantityOfStock)
    {
        $this->productID = $productID;
        $this->sizeID = $sizeID;
        $this->quantityOfStock = $quantityOfStock;
    }

    public function getProductID(){
		return $this->productID;
	}

	public function setProductID($productID){
		$this->productID = $productID;
	}

	public function getSizeID(){
		return $this->sizeID;
	}

	public function setSizeID($sizeID){
		$this->sizeID = $sizeID;
	}

	public function getQuantityOfStock(){
		return $this->quantityOfStock;
	}

	public function setQuantityOfStock($quantityOfStock){
		$this->quantityOfStock = $quantityOfStock;
	}
    /*
    ------------------------------------------------------------------------
    ------------------------------------------------------------------------
    ------------------------------------------------------------------------
    */

    private function getAllSizeProduct(){
        return ProductSize::emptyConstruct()->getAllProductSize();
    }

    public function getAllProductDetail(){
        $data = DB::table($this->table)
        ->leftJoin('productsize',$this->table.'.SizeID', '=', 'productsize.SizeID')        
        ->leftJoin('product',$this->table.'.ProductID', '=', 'product.ProductID')    
        ->select($this->table.'.*','SizeNumber','ProductName')   
        ->get();
        return $data;
    }

    public function getProductDetail($id){
        $data = DB::table($this->table)
        ->leftJoin('productsize',$this->table.'.SizeID', '=', 'productsize.SizeID')        
        ->leftJoin('product',$this->table.'.ProductID', '=', 'product.ProductID')    
        ->select($this->table.'.*','SizeNumber','ProductName')   
        ->where($this->table.'.ProductID', $id)
        ->get();
        return $data;
    }

    public function addNewProductDetail($id){
        $productSizes = $this->getAllSizeProduct();

        foreach ($productSizes as $item) {
            $sizeID = $item->SizeID;
            DB::table($this->table)->insert([
                'ProductID' => $id,
                'SizeID' => $sizeID,
                'QuantityOfStock' => 0,
            ]);
        }
    }

    public function updateQuantityOfStock(){
        $data = DB::table($this->table)
        ->where('SizeID',$this->getSizeID())
        ->where('ProductID',$this->getProductID())
        ->update([ // update
            'QuantityOfStock' => $this->getQuantityOfStock(),
        ]);

        if($data)
            return array(
                'error' => false,
                'message' => 'cập nhật size giày thành công'
            );
        else 
            return array(
                'error' => true,
                'message' => 'cập nhật size giày thất bại'
            );
    }

    public function refreshQuantityOfStock() // get "Quantity Of Stock" when we have productID and sizeID
    {
        $data = DB::table($this->table)
        ->where('ProductID', $this->getProductID())
        ->where('SizeID', $this->getSizeID())
        ->select('QuantityOfStock')
        ->first();
        $this->setQuantityOfStock($data->QuantityOfStock);
    }

    public function minusQuantityOfStock($amount) // minus "Quantity Of Stock" when customer buy product (update QuantityOfStock)
    {
        DB::table($this->table)
        ->where('SizeID',$this->getSizeID())
        ->where('ProductID',$this->getProductID())
        ->update([ // update
            'QuantityOfStock' => $this->getQuantityOfStock() - $amount,
        ]);
    }

    public function checkQuantityOfStock($item)
    {
        $data = DB::table($this->table)
        ->where('SizeID',$item['size_id'])
        ->where('ProductID',$item['product_id'])
        ->first();

        if($data->QuantityOfStock >= $item['amount'])
            return array('checkQuantityOfStock' => true);
        else{
            return array('checkQuantityOfStock' => false, 'item' => $item);
        }
    }
}
