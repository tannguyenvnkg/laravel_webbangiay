<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File; 
use App\Models\ProductDetail;
class Product extends Model
{
    use HasFactory;
    protected $table = 'product';
    private $productName;
    private $price;
    private $image;
    private $updateAt;
    private $providerID;
    private $categoryID;
    private $status;

    /*
    ------------------------------------------------------------------------
    GET SET CONSTRUCT
    ------------------------------------------------------------------------
    */

    public static function emptyConstruct(){return new Product('',0,'',date('Y-m-d H:i:s'),1,1,1);}
    public function __construct($productName,$price,$image,$updateAt,$providerID,$categoryID,$status)
    {
        $this->productName = $productName;
        $this->price = $price;
        $this->image = $image;
        $this->updateAt = $updateAt;
        $this->providerID = $providerID;
        $this->categoryID = $categoryID;
        $this->status = $status;
    }

    public function getProductName(){
		return $this->productName;
	}

	public function setProductName($productName){
		$this->productName = $productName;
	}

	public function getPrice(){
		return $this->price;
	}

	public function setPrice($price){
		$this->price = $price;
	}

	public function getImage(){
		return $this->image;
	}

	public function setImage($image){
		$this->image = $image;
	}

	public function getUpdateAt(){
		return $this->updateAt;
	}

	public function setUpdateAt($updateAt){
		$this->updateAt = $updateAt;
	}

	public function getProviderID(){
		return $this->providerID;
	}

	public function setProviderID($providerID){
		$this->providerID = $providerID;
	}

	public function getCategoryID(){
		return $this->categoryID;
	}

	public function setCategoryID($categoryID){
		$this->categoryID = $categoryID;
	}

	public function getStatus(){
		return $this->status;
	}

	public function setStatus($status){
		$this->status = $status;
	}
    /*
    ------------------------------------------------------------------------
    ------------------------------------------------------------------------
    ------------------------------------------------------------------------
    */
    public function getAllProduct(){
        $data = DB::table($this->table)
        ->leftJoin('typeofshoes',$this->table.'.TypeID', '=', 'typeofshoes.TypeID')        
        ->leftJoin('provider',$this->table.'.ProviderID', '=', 'provider.ProviderID') 
        ->select($this->table.'.*','NameOfType','ProviderName')       
        ->get();
        return $data;
    }
    public function getListProductWithProviderID($id){
        $data = DB::table($this->table)
        ->leftJoin('typeofshoes',$this->table.'.TypeID', '=', 'typeofshoes.TypeID')        
        ->leftJoin('provider',$this->table.'.ProviderID', '=', 'provider.ProviderID') 
        ->where($this->table.'.ProviderID', $id)
        ->select($this->table.'.*','NameOfType','ProviderName')       
        ->get();
        return $data;
    }
    public function getListProductWithCategoryID($id){
        $data = DB::table($this->table)
        ->leftJoin('typeofshoes',$this->table.'.TypeID', '=', 'typeofshoes.TypeID')        
        ->leftJoin('provider',$this->table.'.ProviderID', '=', 'provider.ProviderID') 
        ->where($this->table.'.TypeID', $id)
        ->select($this->table.'.*','NameOfType','ProviderName')       
        ->get();
        return $data;
    }

    public function getDetailProduct($id){
        $data = DB::table($this->table)
        ->leftJoin('typeofshoes',$this->table.'.TypeID', '=', 'typeofshoes.TypeID')        
        ->leftJoin('provider',$this->table.'.ProviderID', '=', 'provider.ProviderID') 
        ->select($this->table.'.*','NameOfType','ProviderName')     
        ->where('ProductID', $id)  
        ->first();
        return $data;
    }

    public function addProduct(){
        $id = DB::table($this->table)->insertGetId([
            'ProductName' => $this->getProductName(),
            'Price' => $this->getPrice(),
            'Image' => $this->getImage(),
            'Update_at' => $this->getUpdateAt(),
            'ProviderID' => $this->getProviderID(),
            'TypeID' => $this->getCategoryID(),
            'Status' => $this->getStatus()
        ]);

        $prodcutDetail = ProductDetail::emptyConstruct();
        $prodcutDetail->addNewProductDetail($id);
        
        if(is_numeric($id))
            return array(
                'error' => false,
                'message' => 'thêm sản phẩm thành công'
            );
        else 
            return array(
                'error' => true,
                'message' => 'thêm sản phẩm thất bại'
            );
    }

    public function editProduct($productId){
        $product = $this->findProduct($productId);
        if($product===null){ // if there is no product found
            return array('error' => true,'message' => 'Không tìm thấy sản phẩm');
        }else{

            if($product->ProductName == $this->getProductName()
            && $product->Price == $this->getPrice()
            && $product->ProviderID == $this->getProviderID()
            && $product->TypeID == $this->getCategoryID()
            && $product->Status == $this->getStatus())
            return array(
                'error' => true,
                'message' => 'cập nhật thất bại, sản phẩm không có thay đổi'
            );

            $data = DB::table($this->table)->where('ProductID',$productId)->update([ // update
                'ProductName' => $this->getProductName(),
                'Price' => $this->getPrice(),
                // 'Image' => $this->getImage(),
                'Update_at' => $this->getUpdateAt(),
                'ProviderID' => $this->getProviderID(),
                'TypeID' => $this->getCategoryID(),
                'Status' => $this->getStatus()
            ]);

            if($data)
                return array(
                    'error' => false,
                    'message' => 'cập nhật sản phẩm thành công'
                );
            else 
                return array(
                    'error' => true,
                    'message' => 'cập nhật sản phẩm thất bại'
                );
        }
    }

    public function findProduct($productId){
        $data = DB::table($this->table)->where('ProductID', $productId)->first();
        return $data;
    }

    public function updateImageProduct($productId){
        $product = $this->findProduct($productId);
        if($product===null){ // if there is no product found
            return array('error' => true,'message' => 'Không tìm thấy sản phẩm');
        }else{
            $data = DB::table($this->table)
            ->where('ProductID',$productId)->update([ // update
                'Image' => $this->getImage(),
            ]);

            $urlImage = $product->Image; // url image
            $image = last(explode('/',$urlImage)); // get image name
            File::delete('images/product_image/'.$image); // delete old image

            if($data)
                return array(
                    'error' => false,
                    'message' => 'cập nhật hình sản phẩm thành công'
                );
            else 
                return array(
                    'error' => true,
                    'message' => 'cập nhật hình sản phẩm thất bại'
                );
        }
    }

    public function active($productId)
    {
        $product = $this->findProduct($productId);
        if($product===null){ // if there is no product found
            return array('error' => true,'message' => 'Không tìm thấy sản phẩm');
        }{
            $data = DB::table($this->table)
            ->where('ProductID',$productId)->update([ // update
                'Status' => true
            ]);

            if($data)
                return array(
                    'error' => false,
                    'message' => 'kích hoạt sản phẩm thành công'
                );
            else 
                return array(
                    'error' => true,
                    'message' => 'kích hoạt sản phẩm thất bại'
                );
        }
    }
    public function deactive($productId)
    {
        $product = $this->findProduct($productId);
        if($product===null){ // if there is no product found
            return array('error' => true,'message' => 'Không tìm thấy sản phẩm');
        }{
            $data = DB::table($this->table)
            ->where('ProductID',$productId)->update([ // update
                'Status' => false
            ]);

            if($data)
                return array(
                    'error' => false,
                    'message' => 'ngừng kinh doanh sản phẩm thành công'
                );
            else 
                return array(
                    'error' => true,
                    'message' => 'ngừng kinh doanh sản phẩm thất bại'
                );
        }
    }
}
