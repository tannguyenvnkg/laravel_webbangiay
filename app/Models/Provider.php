<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Provider extends Model
{
    use HasFactory;

    protected $table = 'provider';
    private $providerName;
    private $address;
    private $phoneNumber;
    private $status;

    /*
    ------------------------------------------------------------------------
    GET SET CONSTRUCT
    ------------------------------------------------------------------------
    */
    public static function emptyConstruct(){return new Provider('','','',1);}
    public function __construct($providerName,$address,$phoneNumber,$status)
    {
        $this->providerName = $providerName;
        $this->address = $address;
        $this->phoneNumber = $phoneNumber;
        $this->status = $status;
    }
   
    public function getProviderName(){
		return $this->providerName;
	}

	public function setProviderName($providerName){
		$this->providerName = $providerName;
	}

    public function getAddress(){
		return $this->address;
	}

	public function setAddress($address){
		$this->address = $address;
	}

	public function getPhoneNumber(){
		return $this->phoneNumber;
	}

	public function setPhoneNumber($phoneNumber){
		$this->phoneNumber = $phoneNumber;
	}

	public function getStatus(){
		return $this->status;
	}

	public function setStatus($status){
		$this->status = $status;
	}
    /*
    ------------------------------------------------------------------------
    ------------------------------------------------------------------------
    ------------------------------------------------------------------------
    */

    public function getAllProvider(){
        $data = DB::table($this->table)->get();
        return $data;
    }
    public function addProvider(){
        $data = DB::table($this->table)->insert([
            'ProviderName' => $this->getProviderName(),
            'Address' => $this->getAddress(),
            'PhoneNumber' => $this->getPhoneNumber(),
            'Status' => $this->getStatus(),
        ]);
        if($data)
            return array(
                'error' => false,
                'message' => 'thêm nhà cung cấp thành công'
            );
        else 
            return array(
                'error' => true,
                'message' => 'thêm nhà cung cấp thất bại'
            );

        return $data;
    }

    public function editProvider($providerId){
        $provider = $this->findProvider($providerId);
        if($provider===null){ // if there is no provider found
            return array('error' => true,'message' => 'Không tìm thấy nhà cung cấp');
        }else{
            if($provider->ProviderName == $this->getProviderName()
            && $provider->Address == $this->getAddress()
            && $provider->PhoneNumber == $this->getPhoneNumber()
            && $provider->Status == $this->getStatus())
            return array(
                'error' => true,
                'message' => 'cập nhật thất bại, nhà cung cấp không có sự thay đổi'
            );

            $data = DB::table($this->table)->where('ProviderID',$providerId)->update([ // update
                'ProviderName' => $this->getProviderName(),
                'Address' => $this->getAddress(),
                'PhoneNumber' => $this->getPhoneNumber(),
                'Status' => $this->getStatus(),
            ]);

            if($data)
                return array(
                    'error' => false,
                    'message' => 'cập nhật nhà cung cấp thành công'
                );
            else 
                return array(
                    'error' => true,
                    'message' => 'cập nhật nhà cung cấp thất bại'
                );
        }
    }

    public function findProvider($providerId){
        $data = DB::table($this->table)->where('ProviderID', $providerId)->first();
        return $data;
    }
}
