<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\File; 
class ProductController extends Controller
{
    private $product;
    public function __construct()
    {
        $this->product =  Product::emptyConstruct();
    }

    //[GET] api/product/getAllProduct
    public function getDetailProduct($id){
        $data = $this->product->getDetailProduct($id);
        return response()->json([
            'error' => false,
            'message' => '',
            'data'=> $data
        ]);
    }
    //[GET] api/product/getAllProduct
    public function getAllProduct(){
        $data = $this->product->getAllProduct();
        return response()->json([
            'error' => false,
            'message' => '',
            'data'=> $data
        ]);
    }

    //[GET] api/product/provider/{id}
    public function getListProductWithProviderID($id){
        $data = $this->product->getListProductWithProviderID($id);
        return response()->json([
            'error' => false,
            'message' => '',
            'data'=> $data
        ]);
    }
    //[GET] api/product/category/{id}
    public function getListProductWithCategoryID($id){
        $data = $this->product->getListProductWithCategoryID($id);
        return response()->json([
            'error' => false,
            'message' => '',
            'data'=> $data
        ]);
    }

    public function getAddProduct(){
        return view('getAddProduct');
    }

    //[POST] api/product/addProduct
    public function addProduct(Request $request){
        if(empty($request->product_name))
        return response()->json([
            'error'=> true,
            'message' => 'Vui lòng nhập tên cho sản phẩm'
        ]);
        $pathImage = null;
        if($request->hasFile('image')){
            $pathImage = $this->uploadImage($request);
        }
        $product = new Product(
            $request->product_name,
            $request->price,
            $pathImage,
            date('Y-m-d H:i:m'),
            $request->provider_id,
            $request->category_id,
            0
        );
        $data = $product->addProduct();
        return response()->json($data);
    }

    private function uploadImage(Request $request){
        $file = $request->file('image');
        $fileName = uniqid() . $file->getClientOriginalName();
        $file->move('images/product_image',$fileName);
        $pathImage = '';
        if($request->secure()){
            $pathImage = 'https://'.$request->getHttpHost().'/images/product_image/'.$fileName;
        }else{
            $pathImage = 'http://'.$request->getHttpHost().'/images/product_image/'.$fileName;
        }
        
        return $pathImage;
    }

    //[PUT] api/product/editProduct
    public function editProduct(Request $request){
        $validateID = $this->isValidIdProduct($request->product_id); // check product's id
        if($validateID['error']) return response()->json($validateID); // if id is not valid return error
        
        $product = new Product(
            $request->product_name,
            $request->price,
            '',
            date('Y-m-d H:i:m'),
            $request->provider_id,
            $request->category_id,
            1
        );


        $data = $product->editProduct($request->product_id);
        
        return response()->json($data);
    }

    // check valid product's id
    private function isValidIdProduct($product_id){
        if(empty($product_id)){
            return array(
                'error' => true,
                'message' => 'Vui lòng điền id sản phẩm',
            );
        }
        else if(!is_numeric($product_id)){
            return array(
                'error' => true,
                'message' => 'id sản phẩm phải là số',
            );
        }
        else return array('error' => false);
    }

    //[PUT] api/product/updateImageProduct
    public function updateImageProduct(Request $request){
        $validateID = $this->isValidIdProduct($request->product_id); // check product's id
        if($validateID['error']) return response()->json($validateID); // if id is not valid return error

        $pathImage = null;
        if($request->hasFile('image')){ // store new image
            $pathImage = $this->uploadImage($request);

            $product = Product::emptyConstruct();
            $product->setImage($pathImage);
            
            $data = $product->updateImageProduct($request->product_id);
            return response()->json($data);
        }else{
            return response()->json([
                'error'=> true,
                'message' => 'Không có hình được tải lên'
            ]);
        }
    }

    /* [PUT] api/product/active/{id}
    */
    public function active($id)
    {
        $validateID = $this->isValidIdProduct($id); // check product's id
        if($validateID['error']) return response()->json($validateID); // if id is not valid return error

        $data = $this->product->active($id);
        return response()->json($data, 200);
    }

    /* [PUT] api/product/deactive/{id}
    */
    public function deactive($id)
    {
        $validateID = $this->isValidIdProduct($id); // check product's id
        if($validateID['error']) return response()->json($validateID); // if id is not valid return error

        $data = $this->product->deactive($id);
        return response()->json($data, 200);
    }
}
