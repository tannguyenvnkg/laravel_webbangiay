<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductSize;

class ProductSizeController extends Controller
{
    private $productSize;
    public function __construct()
    {
        $this->productSize =  ProductSize::emptyConstruct();
    }

    //[GET] api/product-size/get-all-product-size
    public function getAllProductSize(){
        $data = $this->productSize->getAllProductSize();
        return response()->json([
            'error' => false,
            'message' => '',
            'data'=> $data
        ]);
    }

    //[POST] api/product-size/add-product-size
    public function addProductSize(Request $request){
        if(empty($request->size_number))
        return response()->json([
            'error'=> true,
            'message' => 'Vui lòng size sản phẩm'
        ]);

        $productSize = new ProductSize(
            $request->size_number,
        );
        $data = $productSize->addProductSize();
        return response()->json($data);
    }

    //[PUT] api/product-size/edit-product-size
    public function editProductSize(Request $request){
        $validateID = $this->isValidIdProductSize($request->product_size_id); // check productSize's id
        if($validateID['error']) return response()->json($validateID); // if id is not valid return error
        
        $productSize = new ProductSize(
            $request->size_number,
        );

        $data = $productSize->editProductSize($request->product_size_id);
        
        return response()->json($data);
    }

    // check valid productSize's id
    private function isValidIdProductSize($product_size_id){
        if(empty($product_size_id)){
            return array(
                'error' => true,
                'message' => 'Vui lòng điền id size giày',
            );
        }
        else if(!is_numeric($product_size_id)){
            return array(
                'error' => true,
                'message' => 'id size giày phải là số',
            );
        }
        else return array('error' => false);
    }
}
