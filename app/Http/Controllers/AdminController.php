<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
class AdminController extends Controller
{
    private $admin;
    public function __construct()
    {
        $this->admin =  Admin::emptyConstruct();
    }

    /*
        [POST] api/admin/login
        fields: username,password
    */
    public function login(Request $request){
        if(empty($request->username) || empty($request->password))
            return response()->json([
                'error'=> true,
                'message' => 'Vui lòng điền đầy đủ thông tin'
            ]);
        $admin = Admin::emptyConstruct();
        $admin->setUsername($request->username);
        $admin->setPassword(md5($request->password));
        $data = $admin->login();

        if($data === null) {
            return response()->json(
                array(
                'error' => true,
                'message' => 'đăng nhập thất bại, vui lòng kiểm tra lại tài khoản mật khẩu'
            ));
        }
        else{
            return response()->json([
                'error' => false,
                'message' => 'đăng nhập thành công',
                'data'=> $data
            ]);
        }
    }

    /*
        [POST] api/admin/add-new-admin
        fields: username,password,name,role
    */
    public function addnewadmin(Request $request){
        if(empty($request->name) || empty($request->username) || empty($request->password))
            return response()->json([
                'error'=> true,
                'message' => 'tên admin, username và mật khẩu là bắt buộc'
            ]);
        $admin = new Admin(
            $request->username,
            md5($request->password),
            $request->name,
            $request->role,
            1
        );
        $data = $admin->addnewadmin();
        return response()->json($data);
    }

    /*
        [POST] api/admin/deactivate-admin
        fields: logged_admin_username, deactivated_admin_username
    */
    public function deactivateAdmin(Request $request)
    {
        $loggedAdminUsername = $request->logged_admin_username;
        $deactivatedAdminUsername = $request->deactivated_admin_username;
        if(empty($loggedAdminUsername) || empty($deactivatedAdminUsername))
            return response()->json([
                'error'=> true,
                'message' => 'Vui lòng điền đầy đủ id của bạn và id tài khoản cần ngưng hoạt động'
            ]);

        if($loggedAdminUsername===$deactivatedAdminUsername)
        return response()->json([
            'error'=> true,
            'message' => 'Bạn không thể ngưng hoạt động tài khoản của bạn'
        ]);

        $data = $this->admin->deactivateAdmin($loggedAdminUsername,$deactivatedAdminUsername);
        return response()->json($data,200);
    }
}
