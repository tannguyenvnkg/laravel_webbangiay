<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Product;
use App\Models\OrderDetail;
use App\Models\ProductDetail;
use App\Models\ProductSize;
use App\Http\Controllers\OrderDetailController;

class OrderController extends Controller
{
    private $order;
    private $product;
    private $orderDetail;
    private $productDetail;
    private $productSize;

    public function __construct()
    {
        $this->order =  Order::emptyConstruct();
        $this->product = Product::emptyConstruct();
        $this->orderDetail = OrderDetail::emptyConstruct();
        $this->productDetail = ProductDetail::emptyConstruct();
        $this->productSize = ProductSize::emptyConstruct();
    }

    /* [POST] api/order/add
        fields: nameCustomer,email,phone, address, cart[ArrayObject] = [{size_id,product_id,amount}]
    */
    public function addOrder(Request $request){
        $cart = $request->input('cart');
        $nameCustomer = $request->nameCustomer;
        $email = $request->email;
        $phone = $request->phone;
        $address = $request->address;

        if($this->isEmptyFieldsOrder($nameCustomer,$email,$phone,$address)){
            return response()->json([
                'error' => true,
                'message' => 'Vui lòng điền đẩy đủ trường'
            ], 200);
        }

        $checkQuantityOfStock = $this->checkQuantityOfStock($cart);
        if($checkQuantityOfStock['error']){
            return response()->json($checkQuantityOfStock, 200);
        }
        
        $totalBill = $this->getTotalBill($cart);
        $order = new Order(0,null,date('Y-m-d'),null,$totalBill,$nameCustomer,$email,$phone,$address);
        $orderID = $order->addOrderAndGetOrderID(); 
        
        $newCart = $this->addPrice($cart);

        $orderDetail = new OrderDetailController();
        $status = $orderDetail->addOrderDetail($orderID,$newCart);
        if($status){
            return response()->json([
                'error' => false,
                'message' => 'Đặt hàng thành công'
            ], 200);
        }else{
            return response()->json([
                'error' => true,
                'message' => 'Đặt hàng thất bại'
            ], 200);
        }
    }

    private function isEmptyFieldsOrder($nameCustomer,$email,$phone,$address){
        return empty($nameCustomer) || empty($email) || empty($phone) || empty($address);
    }

    private function checkQuantityOfStock($cart)
    {
        foreach ($cart as $item) {
            $check = $this->productDetail->checkQuantityOfStock($item); // true if 'amount' in item is less than quantity of stock
            if(!$check['checkQuantityOfStock']){
                $product = $check['item'];
                $productName = $this->product->findProduct($product['product_id'])->ProductName;
                $productSize = $this->productSize->findProductSize($product['size_id'])->SizeNumber;
                return array(
                    'error' => true,
                    'message' => "Đặt hàng thất bại, sản phẩm '$productName' có size $productSize hiện đang không đủ số lượng bạn yêu cầu"
                );
            }
        }
        return array(
            'error' => false,
            'message' => ""
        );
    }

    private function addPrice($cart){ // add price to current cart
        $newCart = [];
        foreach ($cart as $item) { // loop all product in cart
            $item['price'] = $this->product->getDetailProduct($item['product_id'])->Price; // price per product
            $newCart[] = $item;
        }
        return $newCart;
    }

    private function getTotalBill($cart){
        $totalBill = 0;
        foreach ($cart as $item) { // loop all product in cart
            $productPrice = $this->product->getDetailProduct($item['product_id'])->Price; // price per product
            $totalMoney = $productPrice*$item['amount']; // price*amount
            $totalBill += $totalMoney;
        }
        return $totalBill;
    }
    // check valid productDetail's id
    private function isValidId($id){
        if(empty($id)){
            return array(
                'error' => true,
                'message' => 'Vui lòng điền id',
            );
        }
        else if(!is_numeric($id)){
            return array(
                'error' => true,
                'message' => 'id phải là số',
            );
        }
        else return array('error' => false);
    }

    /*
        [PUT] delivery/delivery-success/{id}
        {id} is order's id
    */
    public function deliverySuccess($id)
    {
        $validateID = $this->isValidId($id); // check product's id
        if($validateID['error']) return response()->json($validateID); // if id is not valid return error

        $data = $this->order->deliverySuccess($id);
        return response()->json($data, 200);
    }

    /*
        [PUT] delivery/delivery-cancel/{id}
        {id} is order's id
    */
    public function deliveryCancel($id)
    {
        $validateID = $this->isValidId($id); // check product's id
        if($validateID['error']) return response()->json($validateID); // if id is not valid return error

        $data = $this->order->deliveryCancel($id);
        return response()->json($data, 200);
    }

    /*
        [GET] order/delivery-status/success
    */
    public function getOrderSuccessfully()
    {
        $data = $this->order->getOrderSuccessfully();
        return response()->json($data, 200);
    }

    /*
        [GET] order/delivery-status/cancel
    */
    public function getOrderCanceled()
    {
        $data = $this->order->getOrderCanceled();
        return response()->json($data, 200);
    }
    /*
        [GET] order/delivery-status/delivering
    */
    public function getOrderdelivering()
    {
        $data = $this->order->getOrderdelivering();
        return response()->json($data, 200);
    }

    /*
        [GET] order/find-order-by-id/{id}
    */
    public function findOrderById($id)
    {
        // $validateID = $this->isValidId($id); // check product's id
        // if($validateID['error']) return response()->json($validateID); // if id is not valid return error

        $data = $this->order->findOrderById($id);
        return response()->json($data, 200);
    }

    /*
        [GET] order/detail-order/{id}
    */
    public function getDetailOrder($id)
    {
        $validateID = $this->isValidId($id); // check product's id
        if($validateID['error']) return response()->json($validateID); // if id is not valid return error

        $data = $this->orderDetail->getDetailOrder($id);
        return response()->json($data, 200);
    }

}
