<?php

namespace App\Http\Controllers;

use App\Models\Provider;
use Illuminate\Http\Request;

class ProviderController extends Controller
{
    private $provider;
    public function __construct()
    {
        $this->provider =  Provider::emptyConstruct();
    }

    //[GET] api/provider/getAllProvider
    public function getAllProvider(){
        $data = $this->provider->getAllProvider();
        return response()->json([
            'error' => false,
            'message' => '',
            'data'=> $data
        ]);
    }

    //[POST] api/provider/addProvider
    public function addProvider(Request $request){
        if(empty($request->provider_name))
        return response()->json([
            'error'=> true,
            'message' => 'Vui lòng nhập tên cho nhà cung cấp'
        ]);

        $provider = new Provider(
            $request->provider_name,
            $request->address,
            $request->phone,
            1
        );
        $data = $provider->addProvider();
        return response()->json($data);
    }

    //[PUT] api/provider/editProvider
    public function editProvider(Request $request){
        $validateID = $this->isValidIdProvider($request->provider_id); // check provider's id
        if($validateID['error']) return response()->json($validateID); // if id is not valid return error
        
        $provider = new Provider(
            $request->provider_name,
            $request->address,
            $request->phone,
            $request->status
        );

        $data = $provider->editProvider($request->provider_id);
        
        return response()->json($data);
    }

    // check valid provider's id
    private function isValidIdProvider($provider_id){
        if(empty($provider_id)){
            return array(
                'error' => true,
                'message' => 'Vui lòng điền id nhà cung cấp',
            );
        }
        else if(!is_numeric($provider_id)){
            return array(
                'error' => true,
                'message' => 'id nhà cung cấp phải là số',
            );
        }
        else return array('error' => false);
    }
}
