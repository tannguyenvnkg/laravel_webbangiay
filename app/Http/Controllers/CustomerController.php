<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderDetail;
use Illuminate\Support\Facades\Mail;
class CustomerController extends Controller
{
    private $customer;
    private $order;
    private $orderDetail;
    public function __construct()
    {
        $this->customer =  Customer::emptyConstruct();
        $this->order =  Order::emptyConstruct();
        $this->orderDetail =  OrderDetail::emptyConstruct();
    }

    //[POST] api/customer/login
    public function login(Request $request){
        if(empty($request->username) || empty($request->password))
            return response()->json([
                'error'=> true,
                'message' => 'Vui lòng điền đầy đủ thông tin'
            ]);
        $customer = Customer::emptyConstruct();
        $customer->setUsername($request->username);
        $customer->setEmail($request->username);
        $customer->setPassword(md5($request->password));
        $data = $customer->login();

        if($data === null) {
            return response()->json(
                array(
                'error' => true,
                'message' => 'đăng nhập thất bại, vui lòng kiểm tra lại tài khoản mật khẩu'
            ));
        }
        else{
            return response()->json([
                'error' => false,
                'message' => 'đăng nhập thành công',
                'data'=> $data
            ]);
        }
    }

    //[POST] api/customer/signup
    public function signup(Request $request){
        if(empty($request->customer_name) || empty($request->username) || empty($request->password))
            return response()->json([
                'error'=> true,
                'message' => 'Vui lòng tên người đăng ký, username và mật khẩu là bắt buộc'
            ]);
        $customer = new Customer(
            $request->customer_name,
            $request->username,
            md5($request->password), // encrypt password
            $request->email,
            $request->address,
            $request->phone_number,
            $request->birthday
        );
        $data = $customer->signup();
        return response()->json($data);
    }

    //[PUT] api/customer/edit-customer
    public function editCustomer(Request $request){
        $validateID = $this->isValidIdProductSize($request->customer_id); // check customer's id
        if($validateID['error']) return response()->json($validateID); // if id is not valid return error
        
        $customer = new Customer( 
            $request->customer_name,
            '', //username
            '', // password 
            // $request->email, //email
            '',
            $request->address,
            $request->phone_number,
            $request->birthday
        );

        $data = $customer->editCustomer($request->customer_id);
        return response()->json($data);
    }

    /*  [PUT] api/customer/change-password
        fields: customer_id,new_password,old_password
    */
    public function changePassword(Request $request){
        $validateID = $this->isValidIdProductSize($request->customer_id); // check customer's id
        if($validateID['error']) return response()->json($validateID); // if id is not valid return error
        
        $customer = Customer::emptyConstruct();
        $customer->setPassword($request->new_password);
        $data = $customer->changePassword($request->customer_id,$request->old_password);
        return response()->json($data);
    }

    // check valid customer's id
    private function isValidIdProductSize($customer_id){
        if(empty($customer_id)){
            return array(
                'error' => true,
                'message' => 'Vui lòng điền id customer',
            );
        }
        else if(!is_numeric($customer_id)){
            return array(
                'error' => true,
                'message' => 'id customer phải là số',
            );
        }
        else return array('error' => false);
    }

    /*  [GET] api/customer/my-order/{id}
        {id} is customer's id
    */
    public function getListCustomerOrder($id)
    {
        $validateID = $this->isValidIdProductSize($id); // check order's id
        if($validateID['error']) return response()->json($validateID); // if id is not valid return error

        $customer = $this->customer->findCustomer($id);
        if($customer===null){ // if there is no customer found
            return response()->json([
                'error' => true,
                'message' => 'Không tìm thấy người dùng'
            ], 200);
        }else{
            $data = $this->order->getListCustomerOrder($id);
            if(empty($data)){
                return response()->json([
                    'error' => false,
                    'message' => '',
                    'data'=> $data
                ], 200);
            }else{
                return response()->json([
                    'error' => true,
                    'message' => 'không có đơn hàng'
                ], 200);
            }
        }
    }

    /*  [GET] api/customer/my-order-detail/{id} 
        {id} is OrderID
    */
    public function getCustomerOrderDetail($id)
    {
        $validateID = $this->isValidIdProductSize($id); // check order's id
        if($validateID['error']) return response()->json($validateID); // if id is not valid return error

        if($this->order->findOrder($id)===null){
            return response()->json([
                'error' => true,
                'message' => 'Không tìm thấy đơn hàng'
            ], 200);
        }else{
            $data = $this->orderDetail->getCustomerOrderDetail($id);
            return response()->json([
                'error' => false,
                'message' => '',
                'data'=> $data
            ], 200);
        }
    }

    /*  [POST] api/customer/forgot-password/receive-otp
        fields: email
    */
    public function receiveOTP(Request $request)
    {
        if(empty($request->email)){
            return response()->json([
                'error' => true,
                'message' => 'Vui lòng điền email'
            ], 200);
        }
        $email = $request->email;
        $OTP = rand(10000,99999);
        $data = $this->customer->addResetCode($email,$OTP);
        if(!$data['error']){
            Mail::send('ContentEmail', array('otp' => $OTP), function($message) use ($email){
                $message->to($email, 'Visitor')->subject('Xác thực mã OTP');
            });
    
            return response()->json([
                'error' => false,
                'message' => 'Gửi mail thành công, vui lòng kiểm tra hòm thư'
            ], 200);
        }else{ // return error
            return response()->json($data, 200);
        }
    }

    /*  [POST] api/customer/forgot-password/check-reset-code
        fields: email,resetCode
    */
    public function checkResetCode(Request $request)
    {
        if(empty($request->email)|| empty($request->resetCode)){
            return response()->json([
                'error' => true,
                'message' => 'Vui lòng điền đẩy đủ 2 trường email và resetCodes'
            ], 200);
        }
        $email = $request->email;
        $resetCode = $request->resetCode;

        $customer = Customer::emptyConstruct();
        $customer->setEmail($email);

        $data = $customer->checkResetCode($resetCode);
        return response()->json($data, 200);
    }


    /*  [PUT] api/customer/forgot-password/reset-password
        fields: email,otp,password
    */
    public function resetPassword(Request $request)
    {
        if(empty($request->email) || empty($request->otp) || empty($request->password)){
            return response()->json([
                'error' => true,
                'message' => 'Vui lòng điền đủ 3 trường email, otp và password'
            ], 200);
        }

        $customer = Customer::emptyConstruct();
        $customer->setEmail($request->email);
        $customer->setPassword($request->password);

        $data = $customer->resetPassword($request->otp);
        return response()->json($data, 200);
    }
}

