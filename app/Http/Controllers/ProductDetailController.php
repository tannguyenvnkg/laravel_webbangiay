<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductDetail;

class ProductDetailController extends Controller
{
    private $productDetail;
    public function __construct()
    {
        $this->productDetail =  ProductDetail::emptyConstruct();
    }
    //[GET] api/product-detail/get-all-product-detail
    public function getAllProductDetail(){
        $data = $this->productDetail->getAllProductDetail();
        return response()->json([
            'error' => false,
            'message' => '',
            'data'=> $data
        ]);
    }

    //[GET] api/product-detail/get-product-detail
    public function getProductDetail($id){
        if(empty($id) || !is_numeric($id)){
            return response()->json([
                'error' => true,
                'message' => 'Đã có lỗi xảy ra, vui lòng kiểm tra lại id sản phẩm',
            ]);
        }
        $data = $this->productDetail->getProductDetail($id);
        return response()->json([
            'error' => false,
            'message' => '',
            'data'=> $data
        ]);
    }

    //[PUT] api/product-detail/update-quantity-of-stock
    public function updateQuantityOfStock(Request $request){
        $validateID = $this->isValidId($request->product_id); // check product's id
        if($validateID['error']) return response()->json($validateID); // if id is not valid return error

        $validateID = $this->isValidId($request->size_id); // check size's id
        if($validateID['error']) return response()->json($validateID); // if id is not valid return error

        $productDetail = new ProductDetail(
            $request->product_id,
            $request->size_id,
            $request->quantity_of_stock
        );

        $data = $productDetail->updateQuantityOfStock();
        return response()->json($data);
    }

    // check valid productDetail's id
    private function isValidId($id){
        if(empty($id)){
            return array(
                'error' => true,
                'message' => 'Vui lòng điền đầy đủ id',
            );
        }
        else if(!is_numeric($id)){
            return array(
                'error' => true,
                'message' => 'id phải là số',
            );
        }
        else return array('error' => false);
    }
}
