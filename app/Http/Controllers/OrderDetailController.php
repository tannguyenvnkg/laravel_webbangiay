<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\OrderDetail;
use App\Models\ProductDetail;
class OrderDetailController extends Controller
{
    private $orderDetail;
    public function __construct()
    {
        $this->orderDetail =  OrderDetail::emptyConstruct();
    }

    public function addOrderDetail($orderID,$cart){
        foreach ($cart as $item) {
            $productID = $item['product_id'];
            $sizeID = $item['size_id'];
            $amount = $item['amount'];
            $price = $item['price'];

            $orderDetail = new OrderDetail($orderID,$productID,$sizeID,$amount,$price);
            $orderDetail->addOrderDetail();

            $productDetail = new ProductDetail($productID,$sizeID,0);
            $productDetail->refreshQuantityOfStock(); // get quantity of stock from database
            $productDetail->minusQuantityOfStock($amount);
        }
        return true;
    }

    
}
