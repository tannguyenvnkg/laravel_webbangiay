<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    private $category;
    public function __construct()
    {
        $this->category =  Category::emptyConstruct();
    }

    //[GET] api/category/getAllCategory
    public function getAllCategory(){
        $data = $this->category->getAllCategory();
        return response()->json([
            'error' => false,
            'message' => '',
            'data'=> $data
        ]);
    }

    //[POST] api/category/addCategory
    public function addCategory(Request $request){
        if(empty($request->category_name))
        return response()->json([
            'error'=> true,
            'message' => 'Vui lòng nhập tên cho thể loại'
        ]);

        $category = new Category(
            $request->category_name,
            1
        );
        $data = $category->addCategory();
        return response()->json($data);
    }

    //[PUT] api/category/editCategory
    public function editCategory(Request $request){
        $validateID = $this->isValidIdCategory($request->category_id); // check category's id
        if($validateID['error']) return response()->json($validateID); // if id is not valid return error
        
        $category = new Category(
            $request->category_name,
            $request->status
        );

        $data = $category->editCategory($request->category_id);
        
        return response()->json($data);
    }

    // check valid category's id
    private function isValidIdCategory($category_id){
        if(empty($category_id)){
            return array(
                'error' => true,
                'message' => 'Vui lòng điền id loại giày',
            );
        }
        else if(!is_numeric($category_id)){
            return array(
                'error' => true,
                'message' => 'id loại giày phải là số',
            );
        }
        else return array('error' => false);
    }

}
