<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProviderController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductSizeController;
use App\Http\Controllers\ProductDetailController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\DB;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('test', function (Request $request) {
    dd($request->secure());
});
Route::prefix('provider')->group(function () {
    Route::get('getAllProvider',[ProviderController::class, 'getAllProvider']);
    Route::post('addProvider',[ProviderController::class, 'addProvider']);
    Route::put('editProvider',[ProviderController::class, 'editProvider']);
});

Route::prefix('category')->group(function () {
    Route::get('getAllCategory',[CategoryController::class, 'getAllCategory']);
    Route::post('addCategory',[CategoryController::class, 'addCategory']);
    Route::put('editCategory',[CategoryController::class, 'editCategory']);
});

Route::prefix('product')->group(function () {
    Route::prefix('getAllProduct')->group(function () {
        Route::get('/',[ProductController::class, 'getAllProduct']);
        Route::get('provider/{id}',[ProductController::class, 'getListProductWithProviderID']);
        Route::get('category/{id}',[ProductController::class, 'getListProductWithCategoryID']);
    });
    Route::get('getDetailProduct/{id}',[ProductController::class, 'getDetailProduct']);
    Route::post('addProduct',[ProductController::class, 'addProduct']);
    Route::put('editProduct',[ProductController::class, 'editProduct']);
    Route::post('updateImageProduct',[ProductController::class, 'updateImageProduct']);
    Route::put('active/{id}',[ProductController::class, 'active']);
    Route::put('deactive/{id}',[ProductController::class, 'deactive']);
    
});

Route::prefix('product-size')->group(function () {
    Route::get('get-all-product-size',[ProductSizeController::class, 'getAllProductSize']);
    Route::post('add-product-size',[ProductSizeController::class, 'addProductSize']);
    Route::put('edit-product-size',[ProductSizeController::class, 'editProductSize']);
});

Route::prefix('product-detail')->group(function () {
    Route::get('get-all-product-detail',[ProductDetailController::class, 'getAllProductDetail']);
    Route::get('get-product-detail/{id}',[ProductDetailController::class, 'getProductDetail']);
    Route::put('update-quantity-of-stock',[ProductDetailController::class, 'updateQuantityOfStock']);
});

Route::prefix('customer')->group(function () {
    Route::post('login', [CustomerController::class, 'login']);
    Route::post('signup', [CustomerController::class, 'signup']);
    Route::put('change-password', [CustomerController::class, 'changePassword']);
    Route::put('edit-customer', [CustomerController::class, 'editCustomer']);
    Route::get('my-order/{id}',[CustomerController::class, 'getListCustomerOrder']);
    Route::get('my-order-detail/{id}',[CustomerController::class, 'getCustomerOrderDetail']);
    Route::prefix('forgot-password')->group(function () {
        Route::post('receive-otp',[CustomerController::class, 'receiveOTP']);
        Route::post('check-reset-code', [CustomerController::class, 'checkResetCode']);
        Route::put('reset-password',[CustomerController::class, 'resetPassword']);
    });
});

Route::prefix('order')->group(function () {
    Route::post('add', [OrderController::class, 'addOrder']);
    Route::get('find-order-by-id/{id}', [OrderController::class, 'findOrderById']);
    Route::get('detail-order/{id}', [OrderController::class, 'getDetailOrder']);
    Route::prefix('delivery-status')->group(function () {
        Route::get('success', [OrderController::class, 'getOrderSuccessfully']);
        Route::get('cancel', [OrderController::class, 'getOrderCanceled']);
        Route::get('delivering', [OrderController::class, 'getOrderdelivering']);
    });
});

Route::prefix('delivery')->group(function () {
   Route::put('delivery-success/{id}', [OrderController::class, 'deliverySuccess']); 
   Route::put('delivery-cancel/{id}', [OrderController::class, 'deliveryCancel']); 
});

Route::prefix('admin')->group(function () {
    Route::post('login', [AdminController::class, 'login']);
    Route::post('add-new-admin', [AdminController::class, 'addNewAdmin']);
    Route::put('deactivate-admin', [AdminController::class, 'deactivateAdmin']);
});